// Component written as function component
import React from "react";

const componentClass = 'banner';

function createMarkup(content) {
  return {__html: content};
}

function Banner(props) {
  const {className} = props;
  let {summary, image, name} = props.data;
  if(image) {
    image = image.medium
  };
  return (
      <div className={`${componentClass} ${className}`}>
        <div className={`${componentClass}__image`}>
          <img src={image} alt={name}/>
        </div>
        <div className={`${componentClass}__content`}>
          <h1 className={`${componentClass}__title`}>{name}</h1>
          <div className={`${componentClass}__description`} dangerouslySetInnerHTML={createMarkup(summary)}></div>
        </div>
      </div>
  );
}

export default Banner;