import Banner from './Banner';
import EpisodeList from './EpisodeList';
import React from "react";

function Show(props) {
  const {tvShow, tvShowEpisodes} = props;

  return (
    <div>
      <Banner data={tvShow} />
      <EpisodeList data={tvShowEpisodes} />
    </div>
  )
}

export default Show;