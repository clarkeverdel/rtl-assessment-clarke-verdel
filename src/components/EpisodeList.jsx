import React from "react";
import {Link} from "react-router-dom";

function EpisodeList(props) {
  const tvShowEpisodes = props.data;

  const episodes = tvShowEpisodes.map((episode, index) => {
    const {id, url, name, season, number} = episode;
    return (
    <tr>
      <td>{name}</td>
      <td>Season {season}</td>
      <td>Episode {number}</td>
      <td><Link to={`/episode/${id}`}>View episode</Link></td>
    </tr>
    );
  });

  return (
    <div className="episodes-list">
      <table>
        {episodes}
      </table>
    </div>
    )
}

export default EpisodeList;