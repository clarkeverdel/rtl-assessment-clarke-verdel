import Banner from './Banner';
import React, {useEffect, useState} from "react";
import conf from "../conf";
import {fetchEpisodeData} from "../actions/tvShow.actions";
import {connect} from "react-redux";
import logo from "../logo.svg";
import {Link} from "react-router-dom";

function Episode(props) {

  const {tvEpisodeData} = useState([]);
  const {tvShowEpisode, match} = props;
  const episodeId = match.params.episodeid;

  useEffect(() => {
        props.fetchEpisodeData(`${conf.API_ENDPOINT}episodes/${episodeId}`);
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <main className="App-main">
        <Banner data={tvShowEpisode}></Banner>
        <div>
          <Link to="/"><a href="#">Go back</a></Link>
        </div>

      </main>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    tvShowEpisode: state.tvShowEpisode,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchEpisodeData: (url) => dispatch(fetchEpisodeData(url)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Episode);