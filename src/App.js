// Written as function
// ES6 Transpiling
import "core-js/stable";
import "regenerator-runtime/runtime";

import conf from './conf';

// Main packages
import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {fetchShowData, fetchEpisodesData} from './actions/tvShow.actions.js';


import Show from './components/Show.jsx';

// Styling & Content
import logo from './logo.svg';
import './scss/styles.scss';

function App(props) {

  //Declare initial state variables
  const {tvShowData, tvEpisodesData} = useState([]);
  const {tvShow, tvShowEpisodes} = props;

  useEffect(() => {
      props.fetchShowData(conf.API_ENDPOINT + 'shows/' + conf.SHOW_ID);
      props.fetchEpisodesData(`${conf.API_ENDPOINT}shows/${conf.SHOW_ID}/episodes`);
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <main className="App-main">
        <Show tvShow={tvShow} tvShowEpisodes={tvShowEpisodes}></Show>
      </main>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    tvShow: state.tvShow,
    tvShowEpisodes: state.tvShowEpisodes,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchShowData: (url) => dispatch(fetchShowData(url)),
    fetchEpisodesData: (url) => dispatch(fetchEpisodesData(url)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
