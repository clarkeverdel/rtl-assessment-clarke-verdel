// React & Routing
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';

// Redux related
import configureStore from './store/configure-store';
import { Provider } from 'react-redux'

// Styling imports
import './scss/pages/index.scss';

// Base app & Components
import App from './App';
import Episode from './components/Episode';

import * as serviceWorker from './serviceWorker';

const store = configureStore({});

ReactDOM.render(
  <Provider className="topLevelApp" store={store}>
    <Router history={createBrowserHistory()}>
      <div>
        <Route exact path="/" component={App}/>
        <Route exact path="/episode/:episodeid" to="/" component={Episode}/>
      </div>
    </Router>
  </Provider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
