export function fetchShowSuccess(showData) {
  return {
    type: 'FETCH_SHOW_SUCCESS',
    showData
  }
}

export function fetchEpisodeSuccess(episodeData) {
  return {
    type: 'FETCH_EPISODE_SUCCESS',
    episodeData
  }
}

export function fetchEpisodesSuccess(episodesData) {
  return {
    type: 'FETCH_EPISODES_SUCCESS',
    episodesData
  }
}

export function fetchShowData(url){
  return (dispatch) => {
    fetch(url)
      .then(res => res.json())
      .then((showData) => {
        dispatch(fetchShowSuccess(showData))
      })
      .catch(err => {
        throw(err);
      });
  }
}

export function fetchEpisodeData(url){
  return (dispatch) => {
    fetch(url)
      .then(res => res.json())
      .then((episodeData) => {
        dispatch(fetchEpisodeSuccess(episodeData))
      })
      .catch(err => {
        throw(err);
      });
  }
}

export function fetchEpisodesData(url){
  return (dispatch) => {
    fetch(url)
      .then(res => res.json())
      .then((episodesData) => {
        dispatch(fetchEpisodesSuccess(episodesData))
      })
      .catch(err => {
        throw(err);
      });
  }
}