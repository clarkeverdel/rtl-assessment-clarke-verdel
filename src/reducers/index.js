import {combineReducers} from 'redux';
import tvShow from './tvShow.reducer.js';
import tvShowEpisode from './tvShowEpisode.reducer.js';
import tvShowEpisodes from './tvShowEpisodes.reducer.js';

const rootReducer = combineReducers({
  tvShow,
  tvShowEpisode,
  tvShowEpisodes
});

export default rootReducer;