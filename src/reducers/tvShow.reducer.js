import * as types from '../actions/types.js';

export default function tvShowReducer(state = [], action) {
  switch (action.type) {
    case types.FETCH_SHOW_SUCCESS:
      return action.showData;
    default:
      return state;
  }
}