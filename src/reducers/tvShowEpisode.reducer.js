import * as types from '../actions/types.js';

export default function tvShowEpisodeReducer(state = [], action) {
  switch (action.type) {
    case types.FETCH_EPISODE_SUCCESS:
      return action.episodeData;
    default:
      return state;
  }
}