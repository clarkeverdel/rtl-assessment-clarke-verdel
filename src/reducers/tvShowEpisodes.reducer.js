import * as types from '../actions/types.js';

export default function tvShowEpisodesReducer(state = [], action) {
  switch (action.type) {
    case types.FETCH_EPISODES_SUCCESS:
      return action.episodesData;
    default:
      return state;
  }
}